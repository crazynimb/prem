<?php 
/*
Plugin Name: Premium Exchanger hooks
Plugin URI: http://best-curs.info
Description: Actions and filters
Version: 0.1
Author: Best-Curs.info
Author URI: http://best-curs.info
*/

if( !defined( 'ABSPATH')){ exit(); }

add_action('wp_footer','my_wp_footer'); 
function my_wp_footer(){
?>

<!-- Put online chat code or another code here / Razmestite kod onlajn chata ili drugoi kod vmesto jetogo teksta !-->

<?php
}

add_filter('general_tech_pages', 'nonnpod_general_tech_pages');
function nonnpod_general_tech_pages($g_pages){
	$g_pages['exchange'] = 'exchange-';
	return $g_pages;
}

add_filter('direction_premalink_temp', 'nonnpod_naps_premalink_temp');
function nonnpod_naps_premalink_temp($temp){
	$temp = '[xmlv1]-to-[xmlv2]';
	return $temp;
}

add_filter('is_direction_premalink', 'nonnpod_is_naps_premalink', 10, 2);
function nonnpod_is_naps_premalink($new_name, $name){
	$new_name = '';
	$new_name = replace_cyr($name);
	$new_name = preg_replace("/[^A-Za-z0-9-]/", '_', $new_name);	
	return $new_name;
}

add_filter('is_direction_name', 'nonnpod_is_naps_chpu', 10, 2);
function nonnpod_is_naps_chpu($new_name, $name){
	$new_name = '';
	if (preg_match("/^[-a-zA-z0-9_]{1,500}$/", $name, $matches )) {
		$new_name = $name;
	} else {
		$new_name = '';
	}	
	return $new_name;
}