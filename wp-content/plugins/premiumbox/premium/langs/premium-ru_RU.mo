��    &      L  5   |      P  u   Q  i   �     1     O     S     Z     p     |     �  
   �     �     �  0   �       
        '     B     R     f     x     �     �     �     �  	   �     �     �     �  
   �     �       	        '     E     c     w     �  �  �  �   �  �   	  2   �	     �	     �	  3   �	     
  1   )
     [
     n
  6   �
  /   �
  X   �
     F     S  %   `     �     �     �     �     �          7     V     t  /   �     �     �     �  3   �          ,  2   :  2   m  ,   �  +   �     �                                    $              	                #         %                
                    &                       !                   "                         %1$s is <strong>deprecated</strong> in plugin <strong>%2$s</strong> since version %3$s with no alternative available. %1$s is <strong>deprecated</strong> in plugin <strong>%2$s</strong> since version %3$s! Use %4$s instead. Action completed successfully All Cancel Choose or upload file Clear field Cron function does not exist Done Error text Error! Action not completed Error! Insufficient privileges Error! Invalid command for task scheduler (cron) Filter First page Insert file into the field Interval 1 hour Interval 10 minutes Interval 12 hours Interval 2 minutes Interval 24 hours Interval 3 hours Interval 30 minutes Interval 5 minutes Last page Maintenance Never No Not action Put in the correct order Save Search... System error (code: anticsfr) System error (code: validreq) Technical page "%s" Title designer When handling Project-Id-Version: Premium
POT-Creation-Date: 2018-10-25 18:26+0300
PO-Revision-Date: 2018-10-25 18:26+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
 %1$s <strong>устарела</strong> в плагине <strong>%2$s</strong> с версии %3$s! Альтернативы нет. %1$s <strong>устарела</strong> в плагине <strong>%2$s</strong> с версии %3$s! Используйте %4$s. Действие успешно выполнено Все Отменить Выберите или загрузите файл Очистить поле Крон функция не существует Выполнено Текст ошибки Ошибка! Действие не выполнено Ошибка! Недостаточно прав Ошибка! Неверная команда для планировщика задач Фильтр Первая Вставьте файл в поле Интервал 1 час Интервал 10 минут Интервал 12 часов Интервал 2 минуты Интервал 24 часа Интервал 3 часа Интервал 30 минут Интервал 5 минут Последняя Техническое обслуживание Никогда Нет Нет действия Расставьте в нужном порядке Сохранить Поиск... Системная ошибка (код: anticsfr) Системная ошибка (код: validreq) Техническая страница "%s" Конструктор заголовков При обращении 